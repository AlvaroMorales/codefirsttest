﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SystemWebCodeFirst.Migrations
{
    public partial class First : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BRAND",
                columns: table => new
                {
                    BRAND_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BRAND = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    BRAND_DESCRIPTION = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    STATUS = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BRAND", x => x.BRAND_ID);
                });

            migrationBuilder.CreateTable(
                name: "COMPUTER",
                columns: table => new
                {
                    COMPUTER_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BRAND_ID = table.Column<int>(nullable: false),
                    MODEL = table.Column<string>(unicode: false, nullable: false),
                    DESCRIPTION = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    STATUS = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_COMPUTER", x => x.COMPUTER_ID);
                    table.ForeignKey(
                        name: "FK_computer_brand",
                        column: x => x.BRAND_ID,
                        principalTable: "BRAND",
                        principalColumn: "BRAND_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_COMPUTER_BRAND_ID",
                table: "COMPUTER",
                column: "BRAND_ID");

            migrationBuilder.CreateIndex(
                name: "IX_COMPUTER_MODEL",
                table: "COMPUTER",
                column: "MODEL",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "COMPUTER");

            migrationBuilder.DropTable(
                name: "BRAND");
        }
    }
}
