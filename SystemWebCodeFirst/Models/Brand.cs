﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemWebCodeFirst.Models
{
    public class Brand
    {
        public int brandId { get; set; }
        [Required(ErrorMessage ="You must choose a brand.")]
        [StringLength(50, MinimumLength =3,
            ErrorMessage ="The name must have the lenght betweeen 3 and 50.")]
        public string brand { get; set; }
        [StringLength(100,ErrorMessage ="The maxlenght of the description must be 100.")]
        public string description { get; set; }
        public bool status { get; set; }
        public virtual ICollection<Computer> computers { get; set; }
    }
}
