﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystemWebCodeFirst.Models
{
    public class DBContextCF : DbContext
    {
        public DBContextCF()
        {
        }
        public DBContextCF(DbContextOptions<DbContext> options) : base(options)
        {

        }
        public DBContextCF(DbContextOptions options) : base(options)
        {
        }
        public virtual DbSet<Brand> brand { get; set; }
        public virtual DbSet<Computer> computer { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brand>(entity =>
            {
                entity.ToTable("BRAND");
                entity.HasKey(e => e.brandId);
                entity.Property(e => e.brandId).HasColumnName("BRAND_ID");

                
                entity.Property(e => e.brand)
                    .HasColumnName("BRAND")
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.description)
                .HasColumnName("BRAND_DESCRIPTION")
                .HasMaxLength(100)
                 .IsUnicode(false);

                entity.Property(e => e.status)
                .HasColumnName("STATUS")
                .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<Computer>(entity =>
            {
                entity.ToTable("COMPUTER");
                entity.HasKey(e => e.computerId);
                entity.Property(e => e.computerId).HasColumnName("COMPUTER_ID");

                entity.Property(e => e.brandId).HasColumnName("BRAND_ID");

                entity.Property(e => e.model)
                .HasColumnName("MODEL")
                .IsRequired()
                .IsUnicode(false);

                entity.HasIndex(e => e.model)
                .IsUnique();

                entity.Property(e => e.description)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(100)
                .IsUnicode(false);
                
                entity.Property(e => e.status)
                .HasColumnName("STATUS")
                .HasDefaultValueSql("((1))");

                entity.HasOne(c => c.brand)
                .WithMany(p => p.computers)
                .HasForeignKey(c => c.brandId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_computer_brand");
            });
        }


    }
}
