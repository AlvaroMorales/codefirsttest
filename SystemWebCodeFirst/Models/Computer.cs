﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SystemWebCodeFirst.Models
{
    public class Computer
    {
        public int computerId { get; set; }
        [Required(ErrorMessage ="You have to choose a branhd.")]
        public int brandId { get; set; }
        [StringLength(30,ErrorMessage ="The maxlenght of the model must be 30.")]
        public string model { get; set; }
        [StringLength(100, ErrorMessage = "The maxlenght of the description must be 100.")]
        public string description { get; set; }
        public bool status { get; set; }
        public virtual Brand brand { get; set; }
    }
}
